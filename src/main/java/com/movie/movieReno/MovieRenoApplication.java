package com.movie.movieReno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieRenoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieRenoApplication.class, args);
	}

}
