package com.movie.movieReno.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "movie_genre")
public class MovieGenre {
	
	@EmbeddedId
	private MovieGenreId id = new MovieGenreId();
	
	@ManyToOne
    @MapsId("mov_id")
	@JoinColumn(name = "mov_id")
	private Movie movGen;
	
	@ManyToOne
    @MapsId("gen_id")
	@JoinColumn(name = "gen_id")
	private Genre genMov;

	public MovieGenre() {
		
	}
	
	public MovieGenre(Movie movGen, Genre genMov) {
		super();
		this.movGen = movGen;
		this.genMov = genMov;
	}

	public Movie getMovie() {
		return movGen;
	}

	public void setMovie(Movie movie) {
		this.movGen = movie;
	}

	public Genre getGenre() {
		return genMov;
	}

	public void setGenre(Genre genre) {
		this.genMov = genre;
	}
	
	
}
