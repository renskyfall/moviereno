package com.movie.movieReno.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieGenreId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "gen_id")
	private Long genId;
	
	@Column(name = "mov_id")
	private Long movId;
	
	public MovieGenreId() {
		super();
	}
	
	public MovieGenreId(Long genId, Long movId) {
		super();
		this.genId = genId;
		this.movId = movId;
	}

	public Long getGenId() {
		return genId;
	}

	public void setGenId(Long genId) {
		this.genId = genId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result
                + ((genId == null) ? 0 : genId.hashCode());
        result = prime * result
                + ((movId == null) ? 0 : movId.hashCode());
        return result;
		
	}
	
	@Override
	public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MovieGenreId other = (MovieGenreId) obj;
        return Objects.equals(getGenId(), other.getGenId()) && Objects.equals(getMovId(), other.getMovId());
    }
}