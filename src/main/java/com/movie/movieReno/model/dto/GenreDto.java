package com.movie.movieReno.model.dto;

public class GenreDto {
	

	private Long id;
	
	private String title;
	
	public GenreDto(Long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
