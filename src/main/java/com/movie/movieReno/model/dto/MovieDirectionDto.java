package com.movie.movieReno.model.dto;

import com.movie.movieReno.model.Director;
import com.movie.movieReno.model.Movie;

public class MovieDirectionDto {
	
	private Movie movie;
	
	private Director director;
	
	public MovieDirectionDto() {
		
	}
	
	public MovieDirectionDto(Movie movie, Director director) {
		super();
		this.movie = movie;
		this.director = director;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}
	
	
}
