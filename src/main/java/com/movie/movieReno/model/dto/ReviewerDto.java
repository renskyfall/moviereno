package com.movie.movieReno.model.dto;

public class ReviewerDto {

	private Long id;
	
	private String reviewerName;

	
	public ReviewerDto() {
		
	}
	
	public ReviewerDto(Long id, String reviewerName) {
		super();
		this.id = id;
		this.reviewerName = reviewerName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}
	
}
