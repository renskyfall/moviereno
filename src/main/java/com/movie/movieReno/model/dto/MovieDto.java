package com.movie.movieReno.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MovieDto {

	private Long id;
	
	private String title;
	
	private int year;
	
	private int time;
	
	private String language;
	
	@JsonFormat(pattern = "yyyy/mm/dd")
	private Date dateRelease;
	
	private String releaseCountry;
	
	public MovieDto() {
		
	}
	
	public MovieDto(Long id, String title, int year, int time, String language, Date dateRelease,
			String releaseCountry) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.time = time;
		this.language = language;
		this.dateRelease = dateRelease;
		this.releaseCountry = releaseCountry;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Date getDateRelease() {
		return dateRelease;
	}

	public void setDateRelease(Date dateRelease) {
		this.dateRelease = dateRelease;
	}

	public String getReleaseCountry() {
		return releaseCountry;
	}

	public void setReleaseCountry(String releaseCountry) {
		this.releaseCountry = releaseCountry;
	}
	
	
}
