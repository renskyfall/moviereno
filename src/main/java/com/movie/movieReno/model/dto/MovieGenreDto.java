package com.movie.movieReno.model.dto;

import com.movie.movieReno.model.Genre;
import com.movie.movieReno.model.Movie;

public class MovieGenreDto {

	private Movie movie;
	
	private Genre genre;

	public MovieGenreDto() {
		
	}
	public MovieGenreDto(Movie movie, Genre genre) {
		super();
		this.movie = movie;
		this.genre = genre;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	
}
