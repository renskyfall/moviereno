package com.movie.movieReno.model.dto;

import com.movie.movieReno.model.Movie;
import com.movie.movieReno.model.Reviewer;

public class RatingDto {
	
	private Reviewer reviewer;
	
	private Movie movie;
    
    private int reviewerStars;
    
    private int numOfRatings;

    
    public RatingDto() {
    	
    }
    
	public RatingDto(Reviewer reviewer, Movie movie, int reviewerStars, int numOfRatings) {
		super();
		this.reviewer = reviewer;
		this.movie = movie;
		this.reviewerStars = reviewerStars;
		this.numOfRatings = numOfRatings;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public int getReviewerStars() {
		return reviewerStars;
	}

	public void setReviewerStars(int reviewerStars) {
		this.reviewerStars = reviewerStars;
	}

	public int getNumOfRatings() {
		return numOfRatings;
	}

	public void setNumOfRatings(int numOfRatings) {
		this.numOfRatings = numOfRatings;
	}
    
    
}
