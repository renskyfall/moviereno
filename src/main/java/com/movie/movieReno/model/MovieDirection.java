package com.movie.movieReno.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;



@Entity
@Table(name = "movie_direction")
public class MovieDirection {
	
	@EmbeddedId
	private MovieDirectionId id = new MovieDirectionId();
	
	@ManyToOne
    @MapsId("dir_id")
    @JoinColumn(name = "dir_id")
    Director dirMov;
 
    @ManyToOne
    @MapsId("mov_id")
    @JoinColumn(name = "mov_id")
    Movie movDir2;
    
    public MovieDirection() {
    	
    }
    
	public MovieDirection(Director dirMov, Movie movDir2) {
		super();
		this.dirMov = dirMov;
		this.movDir2 = movDir2;
	}

	public Director getDirector() {
		return dirMov;
	}

	public void setDirector(Director director) {
		this.dirMov = director;
	}

	public Movie getMovie() {
		return movDir2;
	}

	public void setMovie(Movie movie) {
		this.movDir2 = movie;
	}
	
	
}
