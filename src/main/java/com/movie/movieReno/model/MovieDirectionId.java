package com.movie.movieReno.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieDirectionId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "dir_id")
	private Long dirId;
	
	@Column(name = "mov_id")
	private Long movId;
	
	public MovieDirectionId() {
		super();
	}
	
	public MovieDirectionId(Long genId, Long movId) {
		super();
		this.dirId = genId;
		this.movId = movId;
	}

	public Long getDirId() {
		return dirId;
	}

	public void setDirId(Long genId) {
		this.dirId = genId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result
                + ((dirId == null) ? 0 : dirId.hashCode());
        result = prime * result
                + ((movId == null) ? 0 : movId.hashCode());
        return result;
		
	}
	
	@Override
	public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MovieDirectionId other = (MovieDirectionId) obj;
        return Objects.equals(getDirId(), other.getDirId()) && Objects.equals(getMovId(), other.getMovId());
    }
}