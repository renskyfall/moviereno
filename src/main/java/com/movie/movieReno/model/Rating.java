package com.movie.movieReno.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
public class Rating {
	
	@EmbeddedId
	private RatingId id = new RatingId();
	
	@ManyToOne
    @MapsId("rev_id")
    @JoinColumn(name = "rev_id")
    Reviewer reviewer;
 
    @ManyToOne
    @MapsId("mov_id")
    @JoinColumn(name = "mov_id")
    Movie movie;
    
    private int reviewerStars;
    
    private int numOfRatings;
    
    
    public Rating() {
    	
    }
    
	public Rating(Reviewer reviewer, Movie movie, int reviewerStars, int numOfRatings) {
		super();
		this.reviewer = reviewer;
		this.movie = movie;
		this.reviewerStars = reviewerStars;
		this.numOfRatings = numOfRatings;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public int getReviewerStars() {
		return reviewerStars;
	}

	public void setReviewerStars(int reviewerStars) {
		this.reviewerStars = reviewerStars;
	}

	public int getNumOfRatings() {
		return numOfRatings;
	}

	public void setNumOfRatings(int numOfRatings) {
		this.numOfRatings = numOfRatings;
	}
    
    
}
