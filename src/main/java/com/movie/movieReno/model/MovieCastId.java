package com.movie.movieReno.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MovieCastId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "act_id")
	private Long actId;
	
	@Column(name = "mov_id")
	private Long movId;
	
	public MovieCastId() {
		super();
	}
	
	public MovieCastId(Long actId, Long movId) {
		super();
		this.actId = actId;
		this.movId = movId;
	}

	public Long getActId() {
		return actId;
	}

	public void setActId(Long actId) {
		this.actId = actId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result
                + ((actId == null) ? 0 : actId.hashCode());
        result = prime * result
                + ((movId == null) ? 0 : movId.hashCode());
        return result;
		
	}
	
	@Override
	public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MovieCastId other = (MovieCastId) obj;
        return Objects.equals(getActId(), other.getActId()) && Objects.equals(getMovId(), other.getMovId());
    }
}