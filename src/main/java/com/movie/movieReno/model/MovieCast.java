package com.movie.movieReno.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;



@Entity
@Table(name = "movie_cast")
public class MovieCast {
	
	@EmbeddedId
	private MovieCastId id = new MovieCastId();
	
	@ManyToOne
    @MapsId("act_id")
    @JoinColumn(name = "act_id")
    Actor actorMov;
 
    @ManyToOne
    @MapsId("mov_id")
    @JoinColumn(name = "mov_id")
    Movie movieAct;
	
	private String role;
	
	public MovieCast() {
		
	}
	
	public MovieCast(Actor actorMov, Movie movieAct, String role) {
		super();
		this.actorMov = actorMov;
		this.movieAct = movieAct;
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Actor getActor() {
		return actorMov;
	}

	public void setActor(Actor actor) {
		this.actorMov = actor;
	}

	public Movie getMovie() {
		return movieAct;
	}

	public void setMovie(Movie movie) {
		this.movieAct = movie;
	}
	
	
}
