package com.movie.movieReno.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RatingId implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name = "rev_id")
	private Long revId;
	
	@Column(name = "mov_id")
	private Long movId;
	
	public RatingId() {
		super();
	}

	public RatingId(Long revId, Long movId) {
		super();
		this.revId = revId;
		this.movId = movId;
	}

	public Long getRevId() {
		return revId;
	}

	public void setRevId(Long revId) {
		this.revId = revId;
	}

	public Long getMovId() {
		return movId;
	}

	public void setMovId(Long movId) {
		this.movId = movId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result
                + ((revId == null) ? 0 : revId.hashCode());
        result = prime * result
                + ((movId == null) ? 0 : movId.hashCode());
        return result;
		
	}
	
	@Override
	public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MovieCastId other = (MovieCastId) obj;
        return Objects.equals(getRevId(), other.getActId()) && Objects.equals(getMovId(), other.getMovId());
    }
}
