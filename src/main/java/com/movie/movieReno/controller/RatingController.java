package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.Rating;
import com.movie.movieReno.model.dto.RatingDto;
import com.movie.movieReno.repositories.RatingRepository;

@RestController
@RequestMapping("/api")
public class RatingController {
	
	@Autowired
	RatingRepository ratingRepository;
	
	@GetMapping("/rating")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <RatingDto> ratingDtoList = new ArrayList<RatingDto>();
		for(Rating tempRating : ratingRepository.findAll()){
			Rating rating = tempRating;
			RatingDto ratingDto = new RatingDto(rating.getReviewer(), rating.getMovie(), rating.getReviewerStars(), rating.getNumOfRatings());
			ratingDtoList.add(ratingDto);
		}
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", ratingDtoList.size());
    	process.put("Data", ratingDtoList);
		return process;
	}
	
	@GetMapping("/rating/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long ratingId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Rating rating = ratingRepository.findById(ratingId).orElse(null);
		RatingDto ratingDto = new RatingDto(rating.getReviewer(), rating.getMovie(), rating.getReviewerStars(), rating.getNumOfRatings());
		process.put("Messages", "Read Data Success");
    	process.put("Data", ratingDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/rating/create")
	public HashMap<String, Object> create(@Valid @RequestBody RatingDto ratingDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Rating rating = new Rating(ratingDtoBody.getReviewer(), ratingDtoBody.getMovie(), ratingDtoBody.getReviewerStars(), ratingDtoBody.getNumOfRatings());
		ratingRepository.save(rating);
		process.put("Messages", "Create Data Success");
    	process.put("Data", rating);
		return process;
	}
	
	// Update a Note
    @PutMapping("/rating/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long ratingId, @Valid @RequestBody RatingDto ratingDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Rating tempRating = ratingRepository.findById(ratingId).orElse(null);      
    	//rating.getReviewer(), rating.getMovie(), rating.getReviewerStars(), rating.getNumOfRatings()
    	
        if (ratingDto.getReviewer() != null) {
        	tempRating.setReviewer(ratingDto.getReviewer());
        }
        if (ratingDto.getMovie() != null) {
        	tempRating.setMovie(ratingDto.getMovie());
        }
        if (ratingDto.getReviewerStars() != 0) {
        	tempRating.setReviewerStars(ratingDto.getReviewerStars());
        }
        if (ratingDto.getNumOfRatings() != 0) {
        	tempRating.setNumOfRatings(ratingDto.getNumOfRatings());
        }
        
        ratingRepository.save(tempRating);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempRating);
        return process;
    }
    
    // Delete
    @DeleteMapping("/rating/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long ratingId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Rating rating = ratingRepository.findById(ratingId).orElse(null);

        ratingRepository.delete(rating);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", rating);
    	return process;
    }
    
}
