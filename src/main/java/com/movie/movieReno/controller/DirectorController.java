package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.Director;
import com.movie.movieReno.model.dto.DirectorDto;
import com.movie.movieReno.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	
	@Autowired
	DirectorRepository directorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	DirectorDto convertToDto(Director director) {
		DirectorDto directorDto = modelMapper.map(director, DirectorDto.class);
		return directorDto;
	}
	
	Director convertToEntity(DirectorDto directorDto) {
		Director director = modelMapper.map(directorDto, Director.class);
		return director;
	}

	//===============================================USING MODELMAPPER==================================
	
	@GetMapping("/directors")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <DirectorDto> directorDtoList = new ArrayList<DirectorDto>();
		for(Director tempDirector : directorRepository.findAll()){
			directorDtoList.add(convertToDto(tempDirector));
		}
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", directorDtoList.size());
    	process.put("Data", directorDtoList);
		return process;
	}
	
	@GetMapping("/directors/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long directorId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Director director = directorRepository.findById(directorId).orElse(null);
		process.put("Messages", "Read Data Success");
    	process.put("Data", convertToDto(director));
		return process;
	}
	
	// Create a new Note
	@PostMapping("/directors/create")
	public HashMap<String, Object> create(@Valid @RequestBody DirectorDto directorDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Director director = convertToEntity(directorDtoBody);
		directorRepository.save(director);
		process.put("Messages", "Create Data Success");
    	process.put("Data", director);
		return process;
	}
	
	// Update a Note
    @PutMapping("/directors/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long directorId, @Valid @RequestBody DirectorDto directorDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Director tempDirector = directorRepository.findById(directorId).orElse(null);      
    	
    	directorDto.setId(tempDirector.getId());
        if (directorDto.getFirstName() == null) {
        	directorDto.setFirstName(tempDirector.getFirstName());
        }
        if (directorDto.getLastName() == null) {
        	directorDto.setLastName(tempDirector.getLastName());
        }
        
        tempDirector = convertToEntity(directorDto);
        
        directorRepository.save(tempDirector);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempDirector);
        return process;
    }
    
    // Delete
    @DeleteMapping("/directors/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long directorId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Director director = directorRepository.findById(directorId).orElse(null);

        directorRepository.delete(director);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", director);
    	return process;
    }
    
}
