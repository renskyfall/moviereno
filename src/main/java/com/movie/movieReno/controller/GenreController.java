package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.Genre;
import com.movie.movieReno.model.dto.GenreDto;
import com.movie.movieReno.repositories.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {
	
	@Autowired
	GenreRepository genreRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	GenreDto convertToDto(Genre genre) {
		GenreDto genreDto = modelMapper.map(genre, GenreDto.class);
		return genreDto;
	}
	
	Genre convertToEntity(GenreDto genreDto) {
		Genre genre = modelMapper.map(genreDto, Genre.class);
		return genre;
	}
	
	@GetMapping("/genres")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <GenreDto> genreDtoList = new ArrayList<GenreDto>();
		
		for(Genre tempGenre : genreRepository.findAll()){
			genreDtoList.add(convertToDto(tempGenre));
		}
		
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", genreDtoList.size());
    	process.put("Data", genreDtoList);
		return process;
	}
	
	@GetMapping("/genres/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long actorId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Genre genre = genreRepository.findById(actorId).orElse(null);
		
		process.put("Messages", "Read Data Success");
    	process.put("Data", convertToDto(genre));
		return process;
	}
	
	// Create a new Note
	@PostMapping("/genres/create")
	public HashMap<String, Object> create(@Valid @RequestBody GenreDto genreDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Genre genre = convertToEntity(genreDtoBody);
		genreRepository.save(genre);
		process.put("Messages", "Create Data Success");
    	process.put("Data", genre);
		return process;
	}
	
	// Update a Note
    @PutMapping("/genres/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long genreId, @Valid @RequestBody GenreDto genreDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Genre tempGenre = genreRepository.findById(genreId).orElse(null);      
    	
    	genreDto.setId(tempGenre.getId());
        if (genreDto.getTitle() == null) {
        	genreDto.setTitle(tempGenre.getTitle());
        }
        tempGenre = convertToEntity(genreDto);
        genreRepository.save(tempGenre);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempGenre);
        return process;
    }
    
    // Delete
    @DeleteMapping("/genres/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long genreId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Genre genre = genreRepository.findById(genreId).orElse(null);

        genreRepository.delete(genre);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", genre);
    	return process;
    }
    
}
