package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.MovieCast;
import com.movie.movieReno.model.dto.MovieCastDto;
import com.movie.movieReno.repositories.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {
	
	@Autowired
	MovieCastRepository movieCastRepository;
	
	@GetMapping("/movie_cast")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <MovieCastDto> movieCastDtoList = new ArrayList<MovieCastDto>();
		
		for(MovieCast tempMovieCast : movieCastRepository.findAll()){
			MovieCast movieCast = tempMovieCast;
			MovieCastDto genreDto = new MovieCastDto(movieCast.getActor(), movieCast.getMovie(), movieCast.getRole());
			movieCastDtoList.add(genreDto);
		}
		
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", movieCastDtoList.size());
    	process.put("Data", movieCastDtoList);
		return process;
	}
	
	@GetMapping("/movie_cast/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long actorId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		MovieCast movieCast = movieCastRepository.findById(actorId).orElse(null);
		MovieCastDto movieDto = new MovieCastDto(movieCast.getActor(), movieCast.getMovie(), movieCast.getRole());
		
		process.put("Messages", "Read Data Success");
    	process.put("Data", movieDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/movie_cast/create")
	public HashMap<String, Object> create(@Valid @RequestBody MovieCastDto movieCastDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		MovieCast movieCast = new MovieCast(movieCastDtoBody.getActor(), movieCastDtoBody.getMovie(), movieCastDtoBody.getRole());
		movieCastRepository.save(movieCast);
		process.put("Messages", "Create Data Success");
    	process.put("Data", movieCast);
		return process;
	}
	
	// Update a Note
    @PutMapping("/movie_cast/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long movieCastId, @Valid @RequestBody MovieCastDto movieCastDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	MovieCast tempMovie = movieCastRepository.findById(movieCastId).orElse(null);      

        if (movieCastDto.getActor() != null) {
        	tempMovie.setActor(movieCastDto.getActor());
        }
        if (movieCastDto.getMovie() != null) {
        	tempMovie.setMovie(movieCastDto.getMovie());
        }
        if (movieCastDto.getRole() != null) {
        	tempMovie.setRole(movieCastDto.getRole());
        }
        
        movieCastRepository.save(tempMovie);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempMovie);
        return process;
    }
    
    // Delete
    @DeleteMapping("/movie_cast/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long movieCastId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	MovieCast movieCast = movieCastRepository.findById(movieCastId).orElse(null);

        movieCastRepository.delete(movieCast);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", movieCast);
    	return process;
    }
    
}
