package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.MovieDirection;
import com.movie.movieReno.model.dto.MovieDirectionDto;
import com.movie.movieReno.repositories.MovieDirectionRepository;

@RestController
@RequestMapping("/api")
public class MovieDirectionController {
	
	@Autowired
	MovieDirectionRepository movieDirectionRepository;
	
	@GetMapping("/movie_direction")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <MovieDirectionDto> movieDirectionDtoList = new ArrayList<MovieDirectionDto>();
		
		for(MovieDirection tempMovie : movieDirectionRepository.findAll()){
			MovieDirection movieDirection = tempMovie;
			MovieDirectionDto movieDirectionDto = new MovieDirectionDto(movieDirection.getMovie(),movieDirection.getDirector());
			movieDirectionDtoList.add(movieDirectionDto);
		}
		
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", movieDirectionDtoList.size());
    	process.put("Data", movieDirectionDtoList);
		return process;
	}
	
	@GetMapping("/movie_direction/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long movieDirectionId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		MovieDirection movieDirection = movieDirectionRepository.findById(movieDirectionId).orElse(null);
		MovieDirectionDto movieDirectionDto = new MovieDirectionDto(movieDirection.getMovie(), movieDirection.getDirector());
		
		process.put("Messages", "Read Data Success");
    	process.put("Data", movieDirectionDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/movie_direction/create")
	public HashMap<String, Object> create(@Valid @RequestBody MovieDirectionDto movieDirectionDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		MovieDirection movieDirection = new MovieDirection(movieDirectionDtoBody.getDirector(), movieDirectionDtoBody.getMovie());
		
		movieDirectionRepository.save(movieDirection);
		process.put("Messages", "Create Data Success");
    	process.put("Data", movieDirection);
		return process;
	}
	
	// Update a Note
    @PutMapping("/movie_direction/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long movieDirectionId, @Valid @RequestBody MovieDirectionDto movieDirectionDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	MovieDirection tempMovieDirection = movieDirectionRepository.findById(movieDirectionId).orElse(null);      
    	
        if (movieDirectionDto.getDirector() != null) {
        	tempMovieDirection.setDirector(movieDirectionDto.getDirector());
        }
        if (movieDirectionDto.getMovie() != null) {
        	tempMovieDirection.setMovie(movieDirectionDto.getMovie());
        }
  
        
        movieDirectionRepository.save(tempMovieDirection);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempMovieDirection);
        return process;
    }
    
    // Delete
    @DeleteMapping("/movie_direction/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long movieDirectionId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	MovieDirection movieDirection = movieDirectionRepository.findById(movieDirectionId).orElse(null);

        movieDirectionRepository.delete(movieDirection);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", movieDirection);
    	return process;
    }
    
}
