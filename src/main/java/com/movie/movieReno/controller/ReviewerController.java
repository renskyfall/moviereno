package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.Reviewer;
import com.movie.movieReno.model.dto.ReviewerDto;
import com.movie.movieReno.repositories.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerController {
	
	@Autowired
	ReviewerRepository reviewerRepository;
	
	@GetMapping("/reviewer")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <ReviewerDto> reviewerDtoList = new ArrayList<ReviewerDto>();
		for(Reviewer tempReviewer : reviewerRepository.findAll()){
			Reviewer reviewer = tempReviewer;
			ReviewerDto reviewerDto = new ReviewerDto(reviewer.getId(), reviewer.getReviewerName());
			reviewerDtoList.add(reviewerDto);
		}
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", reviewerDtoList.size());
    	process.put("Data", reviewerDtoList);
		return process;
	}
	
	@GetMapping("/reviewer/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long reviewerId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Reviewer reviewer = reviewerRepository.findById(reviewerId).orElse(null);
		ReviewerDto reviewerDto = new ReviewerDto(reviewer.getId(), reviewer.getReviewerName());
		process.put("Messages", "Read Data Success");
    	process.put("Data", reviewerDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/reviewer/create")
	public HashMap<String, Object> create(@Valid @RequestBody ReviewerDto reviewerDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Reviewer reviewer = new Reviewer(reviewerDtoBody.getId(), reviewerDtoBody.getReviewerName());
		reviewerRepository.save(reviewer);
		process.put("Messages", "Create Data Success");
    	process.put("Data", reviewer);
		return process;
	}
	
	// Update a Note
    @PutMapping("/reviewer/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long reviewerId, @Valid @RequestBody ReviewerDto reviewerDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Reviewer tempReviewer = reviewerRepository.findById(reviewerId).orElse(null);      
    	
        if (reviewerDto.getId() != null) {
        	tempReviewer.setId(reviewerDto.getId());
        }
        if (reviewerDto.getReviewerName() != null) {
        	tempReviewer.setReviewerName(reviewerDto.getReviewerName());
        }
        
        reviewerRepository.save(tempReviewer);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempReviewer);
        return process;
    }
    
    // Delete
    @DeleteMapping("/reviewer/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long reviewerId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Reviewer reviewer = reviewerRepository.findById(reviewerId).orElse(null);

        reviewerRepository.delete(reviewer);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", reviewer);
    	return process;
    }
    
}
