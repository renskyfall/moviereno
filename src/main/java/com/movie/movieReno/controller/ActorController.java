package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.Actor;
import com.movie.movieReno.model.dto.ActorDto;
import com.movie.movieReno.repositories.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {
	
	@Autowired
	ActorRepository actorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	private ActorDto convertToDto(Actor actor) {
		ActorDto actorDto = modelMapper.map(actor, ActorDto.class);
		return actorDto;
	}
	
	private Actor convertToEntity(ActorDto actorDto) {
		Actor actor = modelMapper.map(actorDto, Actor.class);
		return actor;
	}
//===============================================USING MODELMAPPER==================================
	
	@GetMapping("/actor")
	public HashMap<String, Object> getAllMaps(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <ActorDto> actorDtoList = new ArrayList<ActorDto>();
		for(Actor tempActor : actorRepository.findAll()){
			actorDtoList.add(convertToDto(tempActor));
		}
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", actorDtoList.size());
    	process.put("Data", actorDtoList);
		return process;
	}
		
	@GetMapping("/actor/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long actorId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Actor actor = actorRepository.findById(actorId).orElse(null);
		ActorDto actorDto = convertToDto(actor);
		
		process.put("Messages", "Read Data Success");
    	process.put("Data", actorDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/actor/create")
	public HashMap<String, Object> create(@Valid @RequestBody ActorDto actorDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Actor actor = convertToEntity(actorDtoBody);
		actorRepository.save(actor);
		process.put("Messages", "Create Data Success");
    	process.put("Data", actor);
		return process;
	}
	
	// Update a Note
    @PutMapping("/actor/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long actorId, @Valid @RequestBody ActorDto actorDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Actor tempActor = actorRepository.findById(actorId).orElse(null);      
    	
    	actorDto.setId(tempActor.getId());
        if (actorDto.getFirstName() == null) {
        	actorDto.setFirstName(tempActor.getFirstName());
        }
        if (actorDto.getLastName() == null) {
        	actorDto.setLastName(tempActor.getLastName());
        }
        if (actorDto.getGender() == null) {
        	actorDto.setGender(tempActor.getGender());
        }
        
        tempActor = convertToEntity(actorDto);
        
        actorRepository.save(tempActor);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempActor);
        return process;
    }
    
    // Delete
    @DeleteMapping("/actor/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long actorId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Actor actor = actorRepository.findById(actorId).orElse(null);

        actorRepository.delete(actor);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", actor);
    	return process;
    }
    
}
