package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.MovieGenre;
import com.movie.movieReno.model.dto.MovieGenreDto;
import com.movie.movieReno.repositories.MovieGenreRepository;

@RestController
@RequestMapping("/api")
public class MovieGenreController {
	
	@Autowired
	MovieGenreRepository movieGenreRepository;
	
	@GetMapping("/movieGenre")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <MovieGenreDto> movieGenreDtoList = new ArrayList<MovieGenreDto>();
		
		for(MovieGenre tempMovieGenre : movieGenreRepository.findAll()){
			MovieGenre movieGenre = tempMovieGenre;
			MovieGenreDto movieGenreDto = new MovieGenreDto(movieGenre.getMovie(), movieGenre.getGenre());
			movieGenreDtoList.add(movieGenreDto);
		}
		
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", movieGenreDtoList.size());
    	process.put("Data", movieGenreDtoList);
		return process;
	}
	
	@GetMapping("/movieGenre/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long movieGenreId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		MovieGenre movieGenre = movieGenreRepository.findById(movieGenreId).orElse(null);
		MovieGenreDto movieGenreDto = new MovieGenreDto(movieGenre.getMovie(), movieGenre.getGenre());
		
		process.put("Messages", "Read Data Success");
    	process.put("Data", movieGenreDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/movieGenre/create")
	public HashMap<String, Object> create(@Valid @RequestBody MovieGenreDto movieGenreDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		MovieGenre movieGenre = new MovieGenre(movieGenreDtoBody.getMovie(), movieGenreDtoBody.getGenre());
		
		movieGenreRepository.save(movieGenre);
		process.put("Messages", "Create Data Success");
    	process.put("Data", movieGenre);
		return process;
	}
	
	// Update a Note
    @PutMapping("/movieGenre/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long movieDirectionId, @Valid @RequestBody MovieGenreDto movieGenreDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	MovieGenre tempMovieGenre = movieGenreRepository.findById(movieDirectionId).orElse(null);      
    	
        if (movieGenreDto.getMovie() != null) {
        	tempMovieGenre.setMovie(movieGenreDto.getMovie());
        }
        if (movieGenreDto.getGenre() != null) {
        	tempMovieGenre.setGenre(movieGenreDto.getGenre());
        }
  
        movieGenreRepository.save(tempMovieGenre);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempMovieGenre);
        return process;
    }
    
    // Delete
    @DeleteMapping("/movieGenre/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long movieDirectionId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	MovieGenre movieGenre = movieGenreRepository.findById(movieDirectionId).orElse(null);

        movieGenreRepository.delete(movieGenre);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", movieGenre);
    	return process;
    }
    
}
