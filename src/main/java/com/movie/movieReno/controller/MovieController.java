package com.movie.movieReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movie.movieReno.model.Movie;
import com.movie.movieReno.model.dto.MovieDto;
import com.movie.movieReno.repositories.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {
	
	@Autowired
	MovieRepository movieRepository;
	
	@GetMapping("/movies")
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> process = new HashMap<String, Object>();
		List <MovieDto> genreDtoList = new ArrayList<MovieDto>();
		
		for(Movie tempMovie : movieRepository.findAll()){
			Movie movie = tempMovie;
			MovieDto genreDto = new MovieDto(movie.getId(), movie.getTitle(), movie.getYear(), movie.getTime(), movie.getLanguage(), movie.getDateRelease(), movie.getReleaseCountry());
			genreDtoList.add(genreDto);
		}
		
		process.put("Messages", "Read All Data Success");
    	process.put("Total Data", genreDtoList.size());
    	process.put("Data", genreDtoList);
		return process;
	}
	
	@GetMapping("/movies/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long actorId){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Movie movies = movieRepository.findById(actorId).orElse(null);
		MovieDto movieDto = new MovieDto(movies.getId(), movies.getTitle(), movies.getYear(), movies.getTime(), movies.getLanguage(), movies.getDateRelease(), movies.getReleaseCountry());
		
		process.put("Messages", "Read Data Success");
    	process.put("Data", movieDto);
		return process;
	}
	
	// Create a new Note
	@PostMapping("/movies/create")
	public HashMap<String, Object> create(@Valid @RequestBody MovieDto movieDtoBody){
		HashMap<String, Object> process = new HashMap<String, Object>();
		Movie movie = new Movie(movieDtoBody.getId(), movieDtoBody.getTitle(), movieDtoBody.getYear(), movieDtoBody.getTime(), movieDtoBody.getLanguage(), movieDtoBody.getDateRelease(), movieDtoBody.getReleaseCountry());
		movieRepository.save(movie);
		process.put("Messages", "Create Data Success");
    	process.put("Data", movie);
		return process;
	}
	
	// Update a Note
    @PutMapping("/movies/{id}")
    public HashMap<String, Object> update(@PathVariable(value = "id") Long movieId, @Valid @RequestBody MovieDto movieDto) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Movie tempMovie = movieRepository.findById(movieId).orElse(null);      
    	
        if (movieDto.getTitle() != null) {
        	tempMovie.setTitle(movieDto.getTitle());
        }
        if (movieDto.getYear() != 0) {
        	tempMovie.setYear(movieDto.getYear());
        }
        if (movieDto.getTime() != 0) {
        	tempMovie.setTime(movieDto.getTime());
        }
        if (movieDto.getLanguage() != null) {
        	tempMovie.setLanguage(movieDto.getLanguage());
        }
        if (movieDto.getDateRelease() != null) {
        	tempMovie.setDateRelease(movieDto.getDateRelease());
        }
        if (movieDto.getReleaseCountry() != null) {
        	tempMovie.setReleaseCountry(movieDto.getReleaseCountry());
        }
        
        movieRepository.save(tempMovie);
        process.put("Message", "Success Updated Data");
        process.put("Data", tempMovie);
        return process;
    }
    
    // Delete
    @DeleteMapping("/movies/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long genreId) {
    	HashMap<String, Object> process = new HashMap<String, Object>();
    	Movie movie = movieRepository.findById(genreId).orElse(null);

        movieRepository.delete(movie);

        process.put("Messages", "Deleting Data Success");
		process.put("Delete data :", movie);
    	return process;
    }
    
}
