package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.Movie;


@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{
	

}
