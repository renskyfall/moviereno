package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.Rating;


@Repository
public interface RatingRepository extends JpaRepository<Rating, Long>{
	

}
