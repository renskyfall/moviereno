package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.Director;


@Repository
public interface DirectorRepository extends JpaRepository<Director, Long>{
	

}
