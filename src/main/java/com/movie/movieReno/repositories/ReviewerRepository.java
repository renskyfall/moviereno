package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.Reviewer;


@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{
	

}
