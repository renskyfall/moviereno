package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.MovieGenre;


@Repository
public interface MovieGenreRepository extends JpaRepository<MovieGenre, Long>{
	

}
