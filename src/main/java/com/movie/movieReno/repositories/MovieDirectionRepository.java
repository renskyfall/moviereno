package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.MovieDirection;


@Repository
public interface MovieDirectionRepository extends JpaRepository<MovieDirection, Long>{
	

}
