package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.MovieCast;


@Repository
public interface MovieCastRepository extends JpaRepository<MovieCast, Long>{
	

}
