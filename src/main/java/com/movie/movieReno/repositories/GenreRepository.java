package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.Genre;


@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>{
	

}
