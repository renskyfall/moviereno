package com.movie.movieReno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movie.movieReno.model.Actor;


@Repository
public interface ActorRepository extends JpaRepository<Actor, Long>{
	

}
